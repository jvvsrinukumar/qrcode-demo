//
//  QRModel.swift
//  QrCode
//
//  Created by Srinu Kumar Jonnalagadda on 20/10/22.
//

import Foundation

struct QRScanDetails : Codable {
    let success: Bool
    let error : Message?
}

// MARK: - DataClass
struct Message: Codable {
    let message: String?
}
