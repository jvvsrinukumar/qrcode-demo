//
//  MainPageVC.swift
//  QrCode
//
//  Created by Srinu Kumar Jonnalagadda on 19/10/22.
//

import UIKit

class MainPageVC: UIViewController {

    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        let rightBarButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(onScan(_:)))
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let appearance = UINavigationBarAppearance()
        appearance.backgroundColor = .red
        appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]

        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.compactAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        self.navigationItem.title = "Bar Code Scanner"
    }

    @objc func onScan(_ sender:UIBarButtonItem!)
    {
           let vc = QRVC(nibName: "QRVC", bundle: nil)
           vc.onScan =  {(qr)->() in
               self.lblTitle.text = "Scanned Bar Code"
               self.lblMsg.text = qr
              // self.postData(qr)
           }
           self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //Api Call after the scan the bar code
    func postData(_ qr : String) {
        Webservice().load(resource: QRScanDetails.postCode(qr)) { result in
            switch result {
            case .success(let result):
                if (result.success) {
                   
                } else {
                    if (result.error != nil) {
                       
                    }
                }
                print(result)
            case .failure(let error):
                print(error)
            }
        
        }
    }
}
