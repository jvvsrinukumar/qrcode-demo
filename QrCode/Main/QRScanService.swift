//
//  QRScanService.swift
//  QrCode
//
//  Created by Srinu Kumar Jonnalagadda on 20/10/22.
//

import Foundation

extension QRScanDetails {
    
    static func postCode(_ qrString : String )  -> Resource<QRScanDetails> {
        print(qrString)
        var body:Dictionary<String,Any> = Dictionary<String,String>()
       
        body["qrString"] = qrString
        
        print(body)
        let urlStr = baseURl + ""
        guard let url = URL(string: urlStr) else {
            fatalError("URL is incorrect!")
        }
        print(url)
        var resource = Resource<QRScanDetails>(url: url)
        resource.httpMethod = HttpMethod.post
        resource.body = try? JSONSerialization.data(withJSONObject: body)
        
        return resource
    }
}
