//
//  QRVC.swift
//  QrCode
//
//  Created by Srinu Kumar Jonnalagadda on 19/10/22.
//

import UIKit
import AVFoundation

class QRVC: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
  
    @IBOutlet weak var btnRestart: UIButton!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var vwBorder: UIView!
    @IBOutlet weak var vwBottom: UIView!
    var onScan : ((_ qrString :String) -> ())?
    @IBOutlet weak var vwContainner: UIView!
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!

        override func viewDidLoad() {
            super.viewDidLoad()

            view.backgroundColor = UIColor.black
            captureSession = AVCaptureSession()

            guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
            let videoInput: AVCaptureDeviceInput

            do {
                videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
            } catch {
                return
            }

            if (captureSession.canAddInput(videoInput)) {
                captureSession.addInput(videoInput)
            } else {
                failed()
                return
            }

            let metadataOutput = AVCaptureMetadataOutput()

            if (captureSession.canAddOutput(metadataOutput)) {
                captureSession.addOutput(metadataOutput)

                metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                metadataOutput.metadataObjectTypes = [.ean8, .ean13, .pdf417]
            } else {
                failed()
                return
            }

            previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            previewLayer.frame = view.layer.bounds
            previewLayer.videoGravity = .resizeAspectFill
            vwContainner.layer.addSublayer(previewLayer)

            vwContainner.addSubview(self.vwBottom)
            self.vwBottom.layer.masksToBounds = false
            self.vwBottom.layer.shadowRadius = 1
            self.vwBottom.layer.shadowOpacity = 1
            self.vwBottom.layer.shadowColor = UIColor.gray.cgColor
            self.vwBottom.layer.shadowOffset = CGSize(width: 0 , height:1)
            self.vwBottom.layer.cornerRadius = 8
            
            vwBorder.layer.borderColor = UIColor.red.cgColor
            vwBorder.layer.borderWidth = 5
            vwContainner.addSubview(self.vwBorder)
            //self.btnRestart.isHidden = true
            captureSession.startRunning()
        }

        func failed() {
            let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
            captureSession = nil
        }

        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)

            if (captureSession?.isRunning == false) {
                captureSession.startRunning()
            }
        }

        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)

            if (captureSession?.isRunning == true) {
                captureSession.stopRunning()
            }
        }

        func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
            captureSession.stopRunning()

            if let metadataObject = metadataObjects.first {
                guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
                guard let stringValue = readableObject.stringValue else { return }
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                found(code: stringValue)
               // if metadataObject.type == .code128{
//                            let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject
//                            do {
//                                if let validData = readableObject?.stringValue?.data(using: .utf8){
//
//                                    let dict = try JSONDecoder().decode([String:String].self,from:validData)
//                                    //do stuff with dict
//                                    print(dict)
//                                    AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
//                                }
//                            } catch {
//                               // self.btnRestart.isHidden = false
//                                self.lblStatus.text = error.localizedDescription
//                                print(error.localizedDescription)
//                            }
//
//
//                } else {
//
//                }
               
            }

            dismiss(animated: true)
        }

        func found(code: String) {
            self.onScan?(code)
            self.navigationController?.popViewController(animated: true)
            print(code)
        }

        override var prefersStatusBarHidden: Bool {
            return true
        }

        override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
            return .portrait
        }

    @IBAction func onBtnRestart(_ sender: Any) {
        captureSession.startRunning()
        
    }
}
