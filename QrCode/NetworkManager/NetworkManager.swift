//
//  NetworkManager.swift
//  QrCode
//
//  Created by Srinu Kumar Jonnalagadda on 20/10/22.
//

import Foundation
import UIKit
import SwiftyJSON

enum NetworkError: Error {
    case decodingError
    case domainError
    case urlError
}

enum HttpMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
}

struct Resource<T: Codable> {
    let url: URL
    var httpMethod: HttpMethod = .get
    var body: Data? = nil
}

extension Resource {
    init(url: URL) {
        self.url = url
    }
}

class Webservice {
    
    func load<T>(resource: Resource<T>, completion: @escaping (Result<T, NetworkError>) -> Void) {
        
        var request = URLRequest(url: resource.url)
        request.httpMethod = resource.httpMethod.rawValue
        request.httpBody = resource.body
       // request.allHTTPHeaderFields = setApiHeaders()
      //  request.addValue(setApiHeaders(), forHTTPHeaderField: "")
        print("request \(request)")
       // print("request \(setApiHeaders())")
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data, error == nil else {
                completion(.failure(.domainError))
                return
            }
            print("data \(data)")
           // if let value = data {
                let swiftyJsonVar = JSON(data)
           // }
            print(swiftyJsonVar)
            
//            let result = try? JSONDecoder().decode(T.self, from: data)
//            print(result)
//            if let res = result  {
//                DispatchQueue.main.async {
//                    completion(.success(res))
//                }
//            } else {
//                completion(.failure(.decodingError))
//            }
            
            do {
                let decoder = JSONDecoder()
                let result = try decoder.decode(T.self, from: data)
              //  print(result)
               // if let res = result  {
                    DispatchQueue.main.async {
                        completion(.success(result))
                    }
//                } else {
//                    completion(.failure(.decodingError))
//                }
            } catch DecodingError.dataCorrupted(let context) {
                print(context)
            } catch DecodingError.keyNotFound(let key, let context) {
                print("Key '\(key)' not found:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch DecodingError.valueNotFound(let value, let context) {
                print("Value '\(value)' not found:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch DecodingError.typeMismatch(let type, let context) {
                print("Type '\(type)' mismatch:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch {
                print("error: 1233", error)
            }
        
            
        }.resume()
        
    }
    
}

