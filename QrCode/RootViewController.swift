//
//  RootViewController.swift
//  QrCode
//
//  Created by Srinu Kumar Jonnalagadda on 19/10/22.
//

import UIKit


class RootViewController: UIViewController {

    var current : UIViewController!
    
    init() {
        let vc = MainPageVC(nibName: "MainPageVC", bundle: nil)
        let nav = UINavigationController(rootViewController: vc)
        current = nav
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addChild(current)
        self.current.view.frame = self.view.bounds
        self.view.addSubview(current.view)
        self.current.didMove(toParent: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let appearance = UINavigationBarAppearance()
                appearance.backgroundColor = .red
                appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
                appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]

                navigationController?.navigationBar.tintColor = .white
                navigationController?.navigationBar.standardAppearance = appearance
                navigationController?.navigationBar.compactAppearance = appearance
                navigationController?.navigationBar.scrollEdgeAppearance = appearance
    }
    
  

}
